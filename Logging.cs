﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace simpLog
{
    /// <summary>
    /// Required parameters to be included in the app.config are [LogR.LoggingFilePath] and [LogR.LoggingLevel]
    /// </summary>
    public class Logging
    {
        public enum EventType
        {
            Debug = 0,
            Info = 1,
            ProcessingSite = 2,
            Warning = 3,
            Error = 4,
            Critical = 5,
            SendingAlert = 6
        }

        public Logging(string delimeter)
        {
            Delimeter = delimeter;
            LoggingStartTime = DateTime.UtcNow;
        }

        public Logging()
        {
            Delimeter = "~";
            LoggingStartTime = DateTime.UtcNow;
        }

        public string Delimeter { get; set; }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private DateTime _loggingStartTime;
        /// <summary>
        /// [UTC Time] Keeps track of the initial time the object for logging was initialized.
        /// </summary>
        public DateTime LoggingStartTime
        {
            get
            {
                return _loggingStartTime;
            }
            private set
            {
                _loggingStartTime = value;
            }
        }
        /// <summary>
        /// [UTC Time] Keeps track of the last time the an error was processed.
        /// </summary>
        public DateTime LastErrorOccurance { get; private set; }
        //internal readonly string _loggingFilePath = ConfigProperty.GetValue("LogR.LoggingFilePath");
        public string LoggingFilePath {
            get
            {
                return ConfigProperty.GetValue("simpLog.LoggingFilePath");
            }
        }

        //TODO: Expand on generic processing for logging.
        public void TWrite<T>(T exceptionValue)
        {
            WriteToLog(exceptionValue.ToString());
        }

        public void WriteToLog(string message, EventType loggingLevel = EventType.Info)
        {
            var configLoggingLevel = ConfigProperty.GetValue("simpLog.LoggingLevel");

            switch (configLoggingLevel.ToUpper())
            {
                case "ALL":
                case "DEBUG":
                    LogWritter(message, loggingLevel);
                    break;

                case "INFO":
                    if ((int)loggingLevel >= 1)
                    {
                        LogWritter(message, loggingLevel);
                    }
                    break;
                case "ERROR":
                    if ((int)loggingLevel >= 4)
                    {
                        LogWritter(message, loggingLevel);
                    }
                    break;
                case "OFF":

                    break;
                default:

                    break;
            }
        }
               
        public void WriteToLog(string message, Exception baseError, EventType loggingLevel = EventType.Info)
        {
            WriteToLog(
                string.Format("Error Occuring at: {0}{1}{0}{2}{0}[{3}]", Delimeter, baseError.TargetSite.Name, message, baseError.StackTrace),
                loggingLevel);
        }

        public void WriteToLog(Exception ex)
        {
            WriteToLog(ex.Message, EventType.Error);
        }

        public static void WriteToLogDirectly(Exception ex)
        {
            Logging directWrite = new Logging();
            directWrite.WriteToLog(ex.Message, EventType.Error);
        }

        public static void WriteToLogDirectly(string errorMessage)
        {
            Logging directWrite = new Logging();
            directWrite.WriteToLog(errorMessage, EventType.Error);
        }

        internal void LogWritter(string message, EventType loggingLevel = EventType.Info)
        {
            // Check if File Exists (proceed if both are true)
            try
            {
                using (var logFileOutput = new StreamWriter(LoggingFilePath, true))
                {
                    logFileOutput.WriteLine();
                    logFileOutput.Write(string.Format("{0}", loggingLevel) + Delimeter);
                    logFileOutput.Write(string.Format("{0}", message) + Delimeter);
                    logFileOutput.Write("Time: {0}", DateTime.Now);
                    //logFileOutput.WriteLine();
                }

                LastErrorOccurance = DateTime.UtcNow;
            }
            catch (DirectoryNotFoundException noDir) // Check if Directory Exists
            {
                // Create Directory 
                var dirPath = LoggingFilePath.Substring(0, LoggingFilePath.LastIndexOf(@"\", StringComparison.Ordinal));
                WriteToLog("The logging directory has been created, due to it not being found in specified location: " + noDir.Message);
                LoggingDirectoryCreator(dirPath);
            }
            catch (Exception ex) // Other Exceptions - Send to Windows Event Log
            {
                CreateEventLogInstance(ex.ToString());
            }
        }

        private void LoggingDirectoryCreator(string directoryPath)
        {
            try
            {
                Directory.CreateDirectory(directoryPath);
            }
            catch (Exception ex)
            {
                // Send Error to Event Log since Directory cannot be created.
                CreateEventLogInstance(ex.ToString());
            }
        }

        public static void CreateEventLogInstance(string eventMessage)
        {


            var eventSource = Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location);
            var eventLog = "Application";
            var eventType = EventLogEntryType.Error;

            if (!EventLog.SourceExists(eventSource))
            {
                EventLog.CreateEventSource(eventSource, eventLog);
            }

            EventLog.WriteEntry(eventSource, eventMessage);
            EventLog.WriteEntry(eventSource, eventMessage, eventType);
        }
    }
}
