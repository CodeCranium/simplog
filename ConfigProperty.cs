﻿using System;
using System.Configuration;

namespace simpLog
{
    class ConfigProperty
    {
        public static string GetValue(string configSectionNodeName)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            try
            {
                var nodeValue = config.AppSettings.Settings[configSectionNodeName].Value;
                return nodeValue;
            }
            catch (Exception ex)
            {
                Logging.CreateEventLogInstance(String.Format("Issue retrieving configuration for the {0} node in the config file: {1} - {2}", configSectionNodeName, config.FilePath, ex.Message));
                throw ex;
            }
        }
    }
}
