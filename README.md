# README #

This README is a basic overview of the projects utilization.



### How do I get set up? ###

* The LogR library can be used by adding a reference to the .dll in Visual Studio.  Add reference to the 'LogR.dll' and add a using statement to your project - ** using LogR;**  which will then allow you to utilize methods such as 

*[Note:  Make sure to check the methods available in the Class Explorer in Visual Studio (after you have added the reference to the project), so that you can see all available methods.*

* Required parameters to be included in the app.config are [LogR.LoggingFilePath] and [LogR.LoggingLevel] in the 'appSettings' node of the 'configuration' section.

```
#!xml

<configuration>
  <appSettings>
    <add key="LogR.LoggingFilePath" value="C:\Temp\LOG.txt"/>
    <add key="LogR.LoggingLevel" value="ALL"/>
  </appSettings>
</configuration>

```