﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace simpLog.UnitTests
{
    [TestClass]
    public class LoggingUnitTests
    {
        [TestMethod]
        public void ShouldCreateLogging()
        {
            var x = new Logging();
            x.WriteToLog("Hello Test");
            x.TWrite("Hello");
            Assert.IsTrue(System.IO.File.Exists(x.LoggingFilePath));
        }

    }
}
